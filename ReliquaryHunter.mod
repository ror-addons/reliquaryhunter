<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

	<UiMod name="ReliquaryHunter" version="1.0" date="06/22/2011" >
		
		<Author name="Alpha_Male" email="alpha_male@speakeasy.net" />
		<Description text="Displays the location of Exposed Reliquaries on the map in LoTD" />

		<VersionSettings gameVersion="1.4.3" windowsVersion="1.0" savedVariablesVersion="1.0" />

		<WARInfo>
			<Categories>
				<Category name="MAP" />
				<Category name="OTHER" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name="MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>

		<Dependencies>
			<Dependency name="EA_WorldMapWindow" optional="false" forceEnable="true" />
		</Dependencies>

		<Files>
			<File name="textures/ReliquaryHunterTextures.xml" />
			<File name="language/ReliquaryHunterLocalization.lua" />
			<File name="source/ReliquaryHunter.lua" />
			<File name="source/ReliquaryHunter.xml" />

		</Files>

		<SavedVariables>
			<SavedVariable name="ReliquaryHunter.Settings" global="false"/>
		</SavedVariables>
		
		<OnInitialize>
			<CallFunction name="ReliquaryHunter.Start" />
		</OnInitialize>

		<OnShutdown>
			<CallFunction name="ReliquaryHunter.Shutdown" />
		</OnShutdown>

		</UiMod>

</ModuleFile>