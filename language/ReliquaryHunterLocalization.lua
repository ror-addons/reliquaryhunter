--	Title: ReliquaryHunter v1.0
--
--	Author: Alpha_Male (alpha_male@speakeasy.net)
--
--	Description: Displays the location of Exposed Reliquaries on the map in LoTD
--
--	Features: * Displays Exposed Reliquary location markers on World Map when in LoTD or viewing LoTD zone map
--						* Reliquary Marker mouseover tooltips that include:
--							* Description
--							* Exact Location coordinates
--						* Non-LoTD specific World Map window features:
--							* Real time editing of Opacity for entire World Map window
--							* Real time editing of Opacity for entire World Map window background cover
--						* Reliquary Options Button
--							* Located on lower left-hand corner of World Map
--							* Reliquary marker display status indicator
--							* Mouseover for help info
--							* Left-click to quickly toggle visibilty of Reliquary markers on and off
--							* Right-click to access Options Menu
--						* Options Menu includes:
--							* Hide Reliquary markers
--							* Disable Reliquary Marker tooltip display
--							* Non-LoTD specific - Modify the Opacity of the entire World Map Window
--							* Non-LoTD specific - Modify the Opacity of the World Map window background cover
--						* Localization Support (needs translators)
--						* Saves Options between sessions
--
--	Files: \language\ReliquaryHunterLocalization.lua
--				 \source\ReliquaryHunter.lua
--				 \source\ReliquaryHunter.xml
--				 \textures\reliquaryhunter_marker.dds
--				 \textures\reliquaryhunter_marker_no.dds
--				 \textures\ReliquaryHunterTextures.xml
--	 			 \ReliquaryHunter.mod
--	 			 \ReliquaryHunter_Install.txt
--	 			 \ReliquaryHunter_Readme.txt
--			
--	Version History: 1.0 Initial Release
--	 
--  Supported Versions: Warhammer Online v1.4.3
--
--	Dependencies: None
--
--	Future Features: None
--
--	Known Issues:  None
--
--	Additional Credits: None
--
--	Special Thanks:	EA/Mythic for a great game and for releasing the API specs
--								  The War Wiki (www.thewarwiki.com) for being a great source of knowledge for WAR mod development
--									www.curse.com and www.curseforge.com for hosting WAR mod files and projects
--									Ominous Latin Name guild of Gorfang for testing and feedback
--								  Trouble guild of Gorfang for their support
--
--	License/Disclaimers: This addon and it's relevant parts and files are released under the 
--											 GNU General Public License version 3 (GPLv3).
--

------------------------------------------------------------------
----  Global Variables
------------------------------------------------------------------

if not ReliquaryHunter then
	ReliquaryHunter = {}
end

ReliquaryHunter.wstrings = {}

------------------------------------------------------------------
----  ENGLISH
------------------------------------------------------------------

ReliquaryHunter.wstrings[SystemData.Settings.Language.ENGLISH] = {
	TOOLTIP_HEADER 													= L"Exposed Reliquary Location",
	TOOLTIP_REWARDTYPE											= L"Advanced Rewards",
	TOOLTIP_DESC 														= L"Only one of these exists at any time and spawns in a random location, alternating between Order and Destro sides of the zone.",
	TOOLTIP_COORDINATES 										= L"Coordinates: ",
	MOUSEOVER_OPTION_BUTTON_TITLE						= L"Reliquary Hunter Addon",
	MOUSEOVER_OPTION_BUTTON_DESC_INLOTD			= L"Currently IN Land of the Dead",
	MOUSEOVER_OPTION_BUTTON_DESC_NOTINLOTD	= L"Currently NOT IN Land of the Dead",
	MOUSEOVER_OPTION_BUTTON_DESC_LCLICK			= L"Left-click to toggle display of Exposed Reliquary location markers.",
	MOUSEOVER_OPTION_BUTTON_DESC_RCLICK			= L"Right-click to open Options Menu.",
	TITLE_ADDON															= L"Reliquary Hunter",
	OPTIONS_TITLE_MARKER										= L"Reliquary Marker Options:",
	OPTIONS_DISPLAY_MARKERS									= L"Hide Exposed Reliquary Location Markers",
	OPTIONS_DISPLAY_MARKERS_TOOLTIPS				= L"Disable Exposed Reliquary Marker Tooltips",
	OPTIONS_TITLE_MAPWINDOW									= L"World Map Options:",
	OPTIONS_WORLDMAP_OPACITY								= L"Set World Map Opacity",
	OPTIONS_WORLDMAP_BACKGROUND_OPACITY			= L"Set World Map Background Opacity",
}

------------------------------------------------------------------
----  FRENCH
------------------------------------------------------------------

ReliquaryHunter.wstrings[SystemData.Settings.Language.FRENCH] = {
	TOOLTIP_HEADER 													= L"Exposed Reliquary Location", -- Localization Required
	TOOLTIP_REWARDTYPE											= L"Advanced Rewards", -- Localization Required
	TOOLTIP_DESC 														= L"Only one of these exists at any time and spawns in a random location, alternating between Order and Destro sides of the zone.", -- Localization Required
	TOOLTIP_COORDINATES 										= L"Coordinates: ", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_TITLE						= L"Reliquary Hunter Addon", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_INLOTD			= L"Currently IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_NOTINLOTD	= L"Currently NOT IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_LCLICK			= L"Left-click to toggle display of Exposed Reliquary location markers.", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_RCLICK			= L"Right-click to open Options Menu.", -- Localization Required
	TITLE_ADDON															= L"Reliquary Hunter", -- Localization Required
	OPTIONS_TITLE_MARKER										= L"Reliquary Marker Options:", -- Localization Required
	OPTIONS_DISPLAY_MARKERS									= L"Hide Exposed Reliquary Location Markers", -- Localization Required
	OPTIONS_DISPLAY_MARKERS_TOOLTIPS				= L"Disable Exposed Reliquary Marker Tooltips", -- Localization Required
	OPTIONS_TITLE_MAPWINDOW									= L"World Map Options:", -- Localization Required
	OPTIONS_WORLDMAP_OPACITY								= L"Set World Map Opacity", -- Localization Required
	OPTIONS_WORLDMAP_BACKGROUND_OPACITY			= L"Set World Map Background Opacity", -- Localization Required
}

------------------------------------------------------------------
----  GERMAN
------------------------------------------------------------------

ReliquaryHunter.wstrings[SystemData.Settings.Language.GERMAN] = {
	TOOLTIP_HEADER 													= L"Exposed Reliquary Location", -- Localization Required
	TOOLTIP_REWARDTYPE											= L"Advanced Rewards", -- Localization Required
	TOOLTIP_DESC 														= L"Only one of these exists at any time and spawns in a random location, alternating between Order and Destro sides of the zone.", -- Localization Required
	TOOLTIP_COORDINATES 										= L"Coordinates: ", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_TITLE						= L"Reliquary Hunter Addon", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_INLOTD			= L"Currently IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_NOTINLOTD	= L"Currently NOT IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_LCLICK			= L"Left-click to toggle display of Exposed Reliquary location markers.", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_RCLICK			= L"Right-click to open Options Menu.", -- Localization Required
	TITLE_ADDON															= L"Reliquary Hunter", -- Localization Required
	OPTIONS_TITLE_MARKER										= L"Reliquary Marker Options:", -- Localization Required
	OPTIONS_DISPLAY_MARKERS									= L"Hide Exposed Reliquary Location Markers", -- Localization Required
	OPTIONS_DISPLAY_MARKERS_TOOLTIPS				= L"Disable Exposed Reliquary Marker Tooltips", -- Localization Required
	OPTIONS_TITLE_MAPWINDOW									= L"World Map Options:", -- Localization Required
	OPTIONS_WORLDMAP_OPACITY								= L"Set World Map Opacity", -- Localization Required
	OPTIONS_WORLDMAP_BACKGROUND_OPACITY			= L"Set World Map Background Opacity", -- Localization Required
}

------------------------------------------------------------------
----  ITALIAN
------------------------------------------------------------------

ReliquaryHunter.wstrings[SystemData.Settings.Language.ITALIAN] = {
	TOOLTIP_HEADER 													= L"Exposed Reliquary Location", -- Localization Required
	TOOLTIP_REWARDTYPE											= L"Advanced Rewards", -- Localization Required
	TOOLTIP_DESC 														= L"Only one of these exists at any time and spawns in a random location, alternating between Order and Destro sides of the zone.", -- Localization Required
	TOOLTIP_COORDINATES 										= L"Coordinates: ", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_TITLE						= L"Reliquary Hunter Addon", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_INLOTD			= L"Currently IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_NOTINLOTD	= L"Currently NOT IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_LCLICK			= L"Left-click to toggle display of Exposed Reliquary location markers.", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_RCLICK			= L"Right-click to open Options Menu.", -- Localization Required
	TITLE_ADDON															= L"Reliquary Hunter", -- Localization Required
	OPTIONS_TITLE_MARKER										= L"Reliquary Marker Options:", -- Localization Required
	OPTIONS_DISPLAY_MARKERS									= L"Hide Exposed Reliquary Location Markers", -- Localization Required
	OPTIONS_DISPLAY_MARKERS_TOOLTIPS				= L"Disable Exposed Reliquary Marker Tooltips", -- Localization Required
	OPTIONS_TITLE_MAPWINDOW									= L"World Map Options:", -- Localization Required
	OPTIONS_WORLDMAP_OPACITY								= L"Set World Map Opacity", -- Localization Required
	OPTIONS_WORLDMAP_BACKGROUND_OPACITY			= L"Set World Map Background Opacity", -- Localization Required
}

------------------------------------------------------------------
----  SPANISH
------------------------------------------------------------------

ReliquaryHunter.wstrings[SystemData.Settings.Language.SPANISH] = {
	TOOLTIP_HEADER 													= L"Exposed Reliquary Location", -- Localization Required
	TOOLTIP_REWARDTYPE											= L"Advanced Rewards", -- Localization Required
	TOOLTIP_DESC 														= L"Only one of these exists at any time and spawns in a random location, alternating between Order and Destro sides of the zone.", -- Localization Required
	TOOLTIP_COORDINATES 										= L"Coordinates: ", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_TITLE						= L"Reliquary Hunter Addon", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_INLOTD			= L"Currently IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_NOTINLOTD	= L"Currently NOT IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_LCLICK			= L"Left-click to toggle display of Exposed Reliquary location markers.", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_RCLICK			= L"Right-click to open Options Menu.", -- Localization Required
	TITLE_ADDON															= L"Reliquary Hunter", -- Localization Required
	OPTIONS_TITLE_MARKER										= L"Reliquary Marker Options:", -- Localization Required
	OPTIONS_DISPLAY_MARKERS									= L"Hide Exposed Reliquary Location Markers", -- Localization Required
	OPTIONS_DISPLAY_MARKERS_TOOLTIPS				= L"Disable Exposed Reliquary Marker Tooltips", -- Localization Required
	OPTIONS_TITLE_MAPWINDOW									= L"World Map Options:", -- Localization Required
	OPTIONS_WORLDMAP_OPACITY								= L"Set World Map Opacity", -- Localization Required
	OPTIONS_WORLDMAP_BACKGROUND_OPACITY			= L"Set World Map Background Opacity", -- Localization Required
}

------------------------------------------------------------------
----  KOREAN
------------------------------------------------------------------

ReliquaryHunter.wstrings[SystemData.Settings.Language.KOREAN] = {
	TOOLTIP_HEADER 													= L"Exposed Reliquary Location", -- Localization Required
	TOOLTIP_REWARDTYPE											= L"Advanced Rewards", -- Localization Required
	TOOLTIP_DESC 														= L"Only one of these exists at any time and spawns in a random location, alternating between Order and Destro sides of the zone.", -- Localization Required
	TOOLTIP_COORDINATES 										= L"Coordinates: ", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_TITLE						= L"Reliquary Hunter Addon", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_INLOTD			= L"Currently IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_NOTINLOTD	= L"Currently NOT IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_LCLICK			= L"Left-click to toggle display of Exposed Reliquary location markers.", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_RCLICK			= L"Right-click to open Options Menu.", -- Localization Required
	TITLE_ADDON															= L"Reliquary Hunter", -- Localization Required
	OPTIONS_TITLE_MARKER										= L"Reliquary Marker Options:", -- Localization Required
	OPTIONS_DISPLAY_MARKERS									= L"Hide Exposed Reliquary Location Markers", -- Localization Required
	OPTIONS_DISPLAY_MARKERS_TOOLTIPS				= L"Disable Exposed Reliquary Marker Tooltips", -- Localization Required
	OPTIONS_TITLE_MAPWINDOW									= L"World Map Options:", -- Localization Required
	OPTIONS_WORLDMAP_OPACITY								= L"Set World Map Opacity", -- Localization Required
	OPTIONS_WORLDMAP_BACKGROUND_OPACITY			= L"Set World Map Background Opacity", -- Localization Required
}

------------------------------------------------------------------
----  S_CHINESE
------------------------------------------------------------------

ReliquaryHunter.wstrings[SystemData.Settings.Language.S_CHINESE] = {
	TOOLTIP_HEADER 													= L"Exposed Reliquary Location", -- Localization Required
	TOOLTIP_REWARDTYPE											= L"Advanced Rewards", -- Localization Required
	TOOLTIP_DESC 														= L"Only one of these exists at any time and spawns in a random location, alternating between Order and Destro sides of the zone.", -- Localization Required
	TOOLTIP_COORDINATES 										= L"Coordinates: ", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_TITLE						= L"Reliquary Hunter Addon", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_INLOTD			= L"Currently IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_NOTINLOTD	= L"Currently NOT IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_LCLICK			= L"Left-click to toggle display of Exposed Reliquary location markers.", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_RCLICK			= L"Right-click to open Options Menu.", -- Localization Required
	TITLE_ADDON															= L"Reliquary Hunter", -- Localization Required
	OPTIONS_TITLE_MARKER										= L"Reliquary Marker Options:", -- Localization Required
	OPTIONS_DISPLAY_MARKERS									= L"Hide Exposed Reliquary Location Markers", -- Localization Required
	OPTIONS_DISPLAY_MARKERS_TOOLTIPS				= L"Disable Exposed Reliquary Marker Tooltips", -- Localization Required
	OPTIONS_TITLE_MAPWINDOW									= L"World Map Options:", -- Localization Required
	OPTIONS_WORLDMAP_OPACITY								= L"Set World Map Opacity", -- Localization Required
	OPTIONS_WORLDMAP_BACKGROUND_OPACITY			= L"Set World Map Background Opacity", -- Localization Required
}

------------------------------------------------------------------
----  T_CHINESE
------------------------------------------------------------------

ReliquaryHunter.wstrings[SystemData.Settings.Language.T_CHINESE] = {
	TOOLTIP_HEADER 													= L"Exposed Reliquary Location", -- Localization Required
	TOOLTIP_REWARDTYPE											= L"Advanced Rewards", -- Localization Required
	TOOLTIP_DESC 														= L"Only one of these exists at any time and spawns in a random location, alternating between Order and Destro sides of the zone.", -- Localization Required
	TOOLTIP_COORDINATES 										= L"Coordinates: ", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_TITLE						= L"Reliquary Hunter Addon", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_INLOTD			= L"Currently IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_NOTINLOTD	= L"Currently NOT IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_LCLICK			= L"Left-click to toggle display of Exposed Reliquary location markers.", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_RCLICK			= L"Right-click to open Options Menu.", -- Localization Required
	TITLE_ADDON															= L"Reliquary Hunter", -- Localization Required
	OPTIONS_TITLE_MARKER										= L"Reliquary Marker Options:", -- Localization Required
	OPTIONS_DISPLAY_MARKERS									= L"Hide Exposed Reliquary Location Markers", -- Localization Required
	OPTIONS_DISPLAY_MARKERS_TOOLTIPS				= L"Disable Exposed Reliquary Marker Tooltips", -- Localization Required
	OPTIONS_TITLE_MAPWINDOW									= L"World Map Options:", -- Localization Required
	OPTIONS_WORLDMAP_OPACITY								= L"Set World Map Opacity", -- Localization Required
	OPTIONS_WORLDMAP_BACKGROUND_OPACITY			= L"Set World Map Background Opacity", -- Localization Required
}

------------------------------------------------------------------
----  JAPANESE
------------------------------------------------------------------

ReliquaryHunter.wstrings[SystemData.Settings.Language.JAPANESE] = {
	TOOLTIP_HEADER 													= L"Exposed Reliquary Location", -- Localization Required
	TOOLTIP_REWARDTYPE											= L"Advanced Rewards", -- Localization Required
	TOOLTIP_DESC 														= L"Only one of these exists at any time and spawns in a random location, alternating between Order and Destro sides of the zone.", -- Localization Required
	TOOLTIP_COORDINATES 										= L"Coordinates: ", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_TITLE						= L"Reliquary Hunter Addon", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_INLOTD			= L"Currently IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_NOTINLOTD	= L"Currently NOT IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_LCLICK			= L"Left-click to toggle display of Exposed Reliquary location markers.", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_RCLICK			= L"Right-click to open Options Menu.", -- Localization Required
	TITLE_ADDON															= L"Reliquary Hunter", -- Localization Required
	OPTIONS_TITLE_MARKER										= L"Reliquary Marker Options:", -- Localization Required
	OPTIONS_DISPLAY_MARKERS									= L"Hide Exposed Reliquary Location Markers", -- Localization Required
	OPTIONS_DISPLAY_MARKERS_TOOLTIPS				= L"Disable Exposed Reliquary Marker Tooltips", -- Localization Required
	OPTIONS_TITLE_MAPWINDOW									= L"World Map Options:", -- Localization Required
	OPTIONS_WORLDMAP_OPACITY								= L"Set World Map Opacity", -- Localization Required
	OPTIONS_WORLDMAP_BACKGROUND_OPACITY			= L"Set World Map Background Opacity", -- Localization Required
}

------------------------------------------------------------------
----  RUSSIAN
------------------------------------------------------------------

ReliquaryHunter.wstrings[SystemData.Settings.Language.RUSSIAN] = {
	TOOLTIP_HEADER 													= L"Exposed Reliquary Location", -- Localization Required
	TOOLTIP_REWARDTYPE											= L"Advanced Rewards", -- Localization Required
	TOOLTIP_DESC 														= L"Only one of these exists at any time and spawns in a random location, alternating between Order and Destro sides of the zone.", -- Localization Required
	TOOLTIP_COORDINATES 										= L"Coordinates: ", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_TITLE						= L"Reliquary Hunter Addon", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_INLOTD			= L"Currently IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_NOTINLOTD	= L"Currently NOT IN Land of the Dead", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_LCLICK			= L"Left-click to toggle display of Exposed Reliquary location markers.", -- Localization Required
	MOUSEOVER_OPTION_BUTTON_DESC_RCLICK			= L"Right-click to open Options Menu.", -- Localization Required
	TITLE_ADDON															= L"Reliquary Hunter", -- Localization Required
	OPTIONS_TITLE_MARKER										= L"Reliquary Marker Options:", -- Localization Required
	OPTIONS_DISPLAY_MARKERS									= L"Hide Exposed Reliquary Location Markers", -- Localization Required
	OPTIONS_DISPLAY_MARKERS_TOOLTIPS				= L"Disable Exposed Reliquary Marker Tooltips", -- Localization Required
	OPTIONS_TITLE_MAPWINDOW									= L"World Map Options:", -- Localization Required
	OPTIONS_WORLDMAP_OPACITY								= L"Set World Map Opacity", -- Localization Required
	OPTIONS_WORLDMAP_BACKGROUND_OPACITY			= L"Set World Map Background Opacity", -- Localization Required
}