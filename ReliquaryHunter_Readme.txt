Title: ReliquaryHunter v1.0

Author: Alpha_Male (alpha_male@speakeasy.net)

Description: Displays the location of Exposed Reliquaries on the map in LoTD

Features: * Displays Exposed Reliquary location markers on World Map when in LoTD or viewing LoTD zone map
					* Reliquary Marker mouseover tooltips that include:
						* Description
						* Exact Location coordinates
					* Non-LoTD specific World Map window features:
						* Real time editing of Opacity for entire World Map window
						* Real time editing of Opacity for entire World Map window background cover
					* Reliquary Options Button
						* Located on lower left-hand corner of World Map
						* Reliquary marker display status indicator
						* Mouseover for help info
						* Left-click to quickly toggle visibilty of Reliquary markers on and off
						* Right-click to access Options Menu
					* Options Menu includes:
						* Hide Reliquary markers
						* Disable Reliquary Marker tooltip display
						* Non-LoTD specific - Modify the Opacity of the entire World Map Window
						* Non-LoTD specific - Modify the Opacity of the World Map window background cover
					* Localization Support (needs translators)
					* Saves Options between sessions

Files: \language\ReliquaryHunterLocalization.lua
			 \source\ReliquaryHunter.lua
			 \source\ReliquaryHunter.xml
			 \textures\reliquaryhunter_marker.dds
			 \textures\reliquaryhunter_marker_no.dds
			 \textures\ReliquaryHunterTextures.xml
 			 \ReliquaryHunter.mod
 			 \ReliquaryHunter_Install.txt
 			 \ReliquaryHunter_Readme.txt
		
Version History: 1.0 Initial Release

Supported Versions: Warhammer Online v1.4.3

Dependencies: None

Future Features: None

Known Issues:  None

Additional Credits: None

Special Thanks:	EA/Mythic for a great game and for releasing the API specs
							  The War Wiki (www.thewarwiki.com) for being a great source of knowledge for WAR mod development
								www.curse.com and www.curseforge.com for hosting WAR mod files and projects
								Ominous Latin Name guild of Gorfang for testing and feedback
							  Trouble guild of Gorfang for their support

License/Disclaimers: This addon and it's relevant parts and files are released under the 
										 GNU General Public License version 3 (GPLv3).

Additional Notes:  The above information about the addon can also be found in the comment header in the .lua files 
(\source\ReliquaryHunter.lua).

For additional help and support with ReliquaryHunter please visit http://war.curse.com/downloads/war-addons/details/reliquaryhunter.aspx