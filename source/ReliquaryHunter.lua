--	Title: ReliquaryHunter v1.0
--
--	Author: Alpha_Male (alpha_male@speakeasy.net)
--
--	Description: Displays the location of Exposed Reliquaries on the map in LoTD
--
--	Features: * Displays Exposed Reliquary location markers on World Map when in LoTD or viewing LoTD zone map
--						* Reliquary Marker mouseover tooltips that include:
--							* Description
--							* Exact Location coordinates
--						* Non-LoTD specific World Map window features:
--							* Real time editing of Opacity for entire World Map window
--							* Real time editing of Opacity for entire World Map window background cover
--						* Reliquary Options Button
--							* Located on lower left-hand corner of World Map
--							* Reliquary marker display status indicator
--							* Mouseover for help info
--							* Left-click to quickly toggle visibilty of Reliquary markers on and off
--							* Right-click to access Options Menu
--						* Options Menu includes:
--							* Hide Reliquary markers
--							* Disable Reliquary Marker tooltip display
--							* Non-LoTD specific - Modify the Opacity of the entire World Map Window
--							* Non-LoTD specific - Modify the Opacity of the World Map window background cover
--						* Localization Support (needs translators)
--						* Saves Options between sessions
--
--	Files: \language\ReliquaryHunterLocalization.lua
--				 \source\ReliquaryHunter.lua
--				 \source\ReliquaryHunter.xml
--				 \textures\reliquaryhunter_marker.dds
--				 \textures\reliquaryhunter_marker_no.dds
--				 \textures\ReliquaryHunterTextures.xml
--	 			 \ReliquaryHunter.mod
--	 			 \ReliquaryHunter_Install.txt
--	 			 \ReliquaryHunter_Readme.txt
--			
--	Version History: 1.0 Initial Release
--	 
--  Supported Versions: Warhammer Online v1.4.3
--
--	Dependencies: None
--
--	Future Features: None
--
--	Known Issues:  None
--
--	Additional Credits: None
--
--	Special Thanks:	EA/Mythic for a great game and for releasing the API specs
--								  The War Wiki (www.thewarwiki.com) for being a great source of knowledge for WAR mod development
--									www.curse.com and www.curseforge.com for hosting WAR mod files and projects
--									Ominous Latin Name guild of Gorfang for testing and feedback
--								  Trouble guild of Gorfang for their support
--
--	License/Disclaimers: This addon and it's relevant parts and files are released under the 
--											 GNU General Public License version 3 (GPLv3).
--

------------------------------------------------------------------
----  Global Variables
------------------------------------------------------------------
if not ReliquaryHunter then
	ReliquaryHunter = {}
end

if not ReliquaryHunterWindow then
	ReliquaryHunterWindow = {}
end

if not ReliquaryHunterOptionsWindow then
	ReliquaryHunterOptionsWindow = {}
end

-- Addon Info
ReliquaryHunter.Title = L"ReliquaryHunter"
ReliquaryHunter.Version = 1.0

------------------------------------------------------------------
----  Local Variables
------------------------------------------------------------------

-- Localization Text
local LOC_TEXT
if not LOC_TEXT then
	LOC_TEXT = ReliquaryHunter.wstrings[SystemData.Settings.Language.active] 
end

-- Name shortcut variables
local windowName = "ReliquaryHunterWindow"
local windowOptionsName = "ReliquaryHunterOptionsWindow"
local markerToolTipWindowName = "ReliquaryHunterMarkerToolTipWindow"
local optionsButtonName = "ReliquaryHunterOptionsButton"

-- Hook variables used for hooking World Map shown/hidden, World Map
-- modes, and interface shutdown to ReliquaryHunter functions
local hookShown
local hookHidden
local hookShutdownPlayInterface
local hookSetMap

-- LoTD Zone id variable
local idZoneLoTD = 191 --191 lotd

-- Reliquary world coordinates
local dataReliquaries = {{id = 0,  xPos = 20120, yPos = 56671 },
												 {id = 1,  xPos =  7013, yPos = 51959 },
												 {id = 2,  xPos =  6829, yPos = 32652 },
												 {id = 3,  xPos = 12706, yPos = 57628 },
												 {id = 4,  xPos = 16953, yPos = 35742 },
												 {id = 5,  xPos = 17145, yPos = 43277 },
												 {id = 6,  xPos = 27578, yPos = 39338 },
												 {id = 7,  xPos = 23629, yPos = 50639 },
												 {id = 8,  xPos = 12015, yPos = 51915 },
												 {id = 9,  xPos = 29214, yPos = 59518 },
												 {id = 10, xPos = 33240, yPos =  7169 },
												 {id = 11, xPos = 22297, yPos = 22944 },
												 {id = 12, xPos = 15924, yPos =  4964 },
												 {id = 13, xPos =  6164, yPos = 21400 },
												 {id = 14, xPos = 30623, yPos =   949 },
												 {id = 15, xPos = 33956, yPos = 22661 },
												 {id = 16, xPos = 25056, yPos =  8274 },
												 {id = 17, xPos = 19372, yPos = 12973 },
												 {id = 18, xPos = 30049, yPos = 14198 },
												 {id = 19, xPos = 14727, yPos = 19967 },
												}

-- Variable that will be used to associate created marker windows with Reliquary data 
local markerTooltipData = {}

local markerTooltipElements = { "Icon",
																"Header",
																"RewardType",
																"Description",
																"MapCoordinates",
															}

------------------------------------------------------------------
----  Misc Global/Local Variable Initializations and Functions
------------------------------------------------------------------
-- Boolean to track visibility of reliquary markers
local bReliquariesShowing = false

-- Default opacity values for World Map and World Map Background (values defined originally by game)
local opacityDefaultValueWorldMap = 1.0
local opacityDefaultValueWorldMapBackgroundCover = 0.8

-- Opacity value for the Reliquary Marker Tooltip window background
local alphaMarkerTooltip = 0.9

-- Custom Tooltip Definitions
ReliquaryHunter.Tooltips = {}
ReliquaryHunter.Tooltips.ANCHOR_ICON_WINDOW_RIGHT_OFFSET = { Point = "topright", 
																														 RelativeTo = "", 
																														 RelativePoint = "topleft", 
																														 XOffset = 0, 
																														 YOffset = -32 }

------------------------------------------------------------------
----  Core Functions
------------------------------------------------------------------

function ReliquaryHunter.Start()
	RegisterEventHandler( SystemData.Events.ENTER_WORLD, "ReliquaryHunter.Initialize" )
	RegisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "ReliquaryHunter.Initialize" )
end

function ReliquaryHunter.Initialize()
		TextLogAddEntry( "Chat", 0, ReliquaryHunter.Title..L" Version "..(wstring.format( L"%.01f",ReliquaryHunter.Version))..L" Initialized" )
		DEBUG ( towstring( "ReliquaryHunter loaded" ))

		-- Initialize saved data (SavedVariables.lua).
		-- Options settings
		if not ReliquaryHunter.Settings then
				ReliquaryHunter.Settings = {
						Version = ReliquaryHunter.Version,
						HideReliquaryMarkers = false,
						DisableReliquaryToolTips = false,
						WorldMapOpacityValueEnabled = false,
						WorldMapOpacityValue = opacityDefaultValueWorldMap,
						WorldMapBackgroundCoverOpacityValueEnabled = false,
						WorldMapBackgroundCoverOpacityValue = opacityDefaultValueWorldMapBackgroundCover,
				}
		end

		-- Set boolean for if Reliquary spawn markers should be showing based on saved settings
		bReliquariesShowing  = not ReliquaryHunter.Settings.HideReliquaryMarkers

		-- Unregister the initial start up event handlers.
		UnregisterEventHandler( SystemData.Events.ENTER_WORLD, "ReliquaryHunter.Initialize" )
		UnregisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "ReliquaryHunter.Initialize" )
		
		-- ReliquaryHunter has now initialized so initialize the ReliquaryHunter related windows 
		ReliquaryHunterWindow.Initialize()

		-- Initialize the ReliquaryHunter Marker Tooltip Window
		ReliquaryHunter.InitializeMarkerTooltip()

		-- Initialize the ReliquaryHunter Option Window
		ReliquaryHunterOptionsWindow.Initialize()
		
end

function ReliquaryHunterWindow.Initialize()
		-- Hooks into the World Map window
		hookShown = EA_Window_WorldMap.OnShown
		hookHidden = EA_Window_WorldMap.OnHidden
		
		EA_Window_WorldMap.OnShown = ReliquaryHunter.OnShown
		EA_Window_WorldMap.OnHidden = ReliquaryHunter.OnHidden
		
		-- Hooks into function that changes map display
		hookSetMap = EA_Window_WorldMap.SetMap
		EA_Window_WorldMap.SetMap = ReliquaryHunter.OnSetMap

		-- Hooks into Play Interface Shutdown
		hookShutdownPlayInterface = InterfaceCore.ShutdownPlayInterface
		InterfaceCore.ShutdownPlayInterface = ReliquaryHunter.Shutdown
end

function ReliquaryHunter.OnShown( ... )

		hookShown( ... )
		
		local parentWindow = "EA_Window_WorldMapZoneView"
		local mapDisplay = "EA_Window_WorldMapZoneViewMapDisplay"
		local mapDisplayWidth, mapDisplayHeight = WindowGetDimensions( mapDisplay )
		
		-- Create the Options window button
		if not DoesWindowExist( optionsButtonName ) then
				CreateWindowFromTemplate( optionsButtonName, "ReliquaryHunterOptionsButtonTemplate", parentWindow )
				WindowSetShowing( optionsButtonName, true )
		
				WindowClearAnchors( optionsButtonName )
				WindowAddAnchor( optionsButtonName, "bottomleft", parentWindow, "bottomleft", 58, -14)
				
				LabelSetText( optionsButtonName.."Label", L"Exposed Reliquary Locations" )
		end

		-- Check if the zone the player is in is LoTD, if not return out immediately
		if ( GameData.Player.zone == idZoneLoTD) then
				
			-- Create a marker windows and display it for each defined Reliquary
			for _, rData in pairs( dataReliquaries ) do

					local markerWindowName = windowName .. "ReliquaryMarker" .. rData.id

					if not DoesWindowExist(markerWindowName) then
							CreateWindowFromTemplate( markerWindowName, "ReliquaryHunterMarkerWindowTemplate", mapDisplay )
							
							WindowClearAnchors( markerWindowName )
							WindowAddAnchor( markerWindowName, "topleft", mapDisplay, "center", (rData.xPos / 65535) * mapDisplayWidth, (rData.yPos / 65535) * mapDisplayHeight )

							-- Associate created marker window with coordinate data
							markerTooltipData[rData.id] = { winName = markerWindowName, xPos = rData.xPos, yPos = rData.yPos }

					end

					-- Display the Reliquary Markers depending upon display status
					if (bReliquariesShowing == true) then
							WindowSetShowing( markerWindowName, true )
					else
							WindowSetShowing( markerWindowName, false )
					end

					-- Allow input (enable tooltips) on Reliquary Markers depending upon the options value
					if ReliquaryHunter.Settings.DisableReliquaryToolTips then
							WindowSetHandleInput(markerWindowName, false)
					else
							WindowSetHandleInput(markerWindowName, true)
					end
			
			end

		end

		-- Dynamically set the Option Button icon depending on zone
		ReliquaryHunter.SetOptionButtonIcon()

end

function ReliquaryHunter.OnSetMap ( ... )

		hookSetMap( ... )

		-- If player is in LoTD then set the showing state of the Reliquary markers
		if ((EA_Window_WorldMap.currentMap == idZoneLoTD) and 
				(GameData.Player.zone == idZoneLoTD) and 
				(ReliquaryHunter.Settings.HideReliquaryMarkers == false)) then
				bReliquariesShowing = true
		else
				bReliquariesShowing = false
		end

		-- If the Options menu is open, make sure the toggle affects the related marker display option for consistency
		ButtonSetPressedFlag( windowOptionsName.."MarkerDisplayButton", ReliquaryHunter.Settings.HideReliquaryMarkers )
		
		for _, rData in pairs( dataReliquaries ) do
		
				local markerWindowName = windowName .. "ReliquaryMarker" .. rData.id
				
				-- Check if window exists to handle initial World Map display and UI reload
				if DoesWindowExist(markerWindowName) then
						if (bReliquariesShowing == true) then
								WindowSetShowing( markerWindowName, true )
						else
								WindowSetShowing( markerWindowName, false )
						end
				end
		
		end
		
		-- Check if Options button exists to handle initial World Map display and UI reload
		if DoesWindowExist(optionsButtonName) then
				ReliquaryHunter.SetOptionButtonIcon()
		end

end

function ReliquaryHunter.ToggleMarkersShowing()

		-- Do not take any action if not viewing LoTD zone map
		if (EA_Window_WorldMap.currentMap == idZoneLoTD) then
				if (bReliquariesShowing == true) then
						bReliquariesShowing = false
						ReliquaryHunter.Settings.HideReliquaryMarkers = true
				else
						bReliquariesShowing = true
						ReliquaryHunter.Settings.HideReliquaryMarkers = false
				end
		
				-- If the Options menu is open, make sure the toggle affects the related marker display option for consistency
				ButtonSetPressedFlag( windowOptionsName.."MarkerDisplayButton", ReliquaryHunter.Settings.HideReliquaryMarkers )
				
				for _, rData in pairs( dataReliquaries ) do
				
						local markerWindowName = windowName .. "ReliquaryMarker" .. rData.id
						
						if (bReliquariesShowing == true) then
								WindowSetShowing( markerWindowName, true )
						else
								WindowSetShowing( markerWindowName, false )
						end
						
				end
		
				-- Dynamically set the Option Button icon depending on zone		
				ReliquaryHunter.SetOptionButtonIcon()
		end
	
end

function ReliquaryHunter.SetOptionButtonIcon()
		-- Change the button icon depending on if in LoTD
		if ( (GameData.Player.zone == idZoneLoTD) and (bReliquariesShowing == true) ) then
				DynamicImageSetTexture ( optionsButtonName.."Icon", "reliquaryhunter_marker", 0, 0 )
		else
				DynamicImageSetTexture ( optionsButtonName.."Icon", "reliquaryhunter_marker_no", 0, 0 )
		end
end

function ReliquaryHunter.OnHidden( ... )
		hookHidden( ... )
end

function ReliquaryHunter.Shutdown( ... )
		return hookShutdownPlayInterface( ... )
end

------------------------------------------------------------------
---- Options Button and Window
------------------------------------------------------------------

function ReliquaryHunter.OnLButtonUpOptionButton()
		ReliquaryHunter.ToggleMarkersShowing()
end

function ReliquaryHunter.OnRButtonUpOptionButton()
		ReliquaryHunterOptionsWindow.ToggleOptions()
end

function ReliquaryHunterOptionsWindow.ToggleOptions()
    local showing = WindowGetShowing( windowOptionsName )
    WindowSetShowing( windowOptionsName, showing == false )
end

function ReliquaryHunterOptionsWindow.Initialize()
		-- Create the Options Window
		CreateWindow( windowOptionsName, false )
		
		-- Options Window Labels
		LabelSetText( windowOptionsName.."TitleBarText", LOC_TEXT.TITLE_ADDON..L" "..GetString( StringTables.Default.LABEL_CHAT_OPTIONS ))   
		LabelSetText( windowOptionsName.."MarkerTitle", LOC_TEXT.OPTIONS_TITLE_MARKER )
		LabelSetText( windowOptionsName.."MarkerDisplayLabel", LOC_TEXT.OPTIONS_DISPLAY_MARKERS )
		LabelSetText( windowOptionsName.."MarkerTooltipDisplayLabel", LOC_TEXT.OPTIONS_DISPLAY_MARKERS_TOOLTIPS )
		LabelSetText( windowOptionsName.."WorldMapWindowTitle", LOC_TEXT.OPTIONS_TITLE_MAPWINDOW )
		LabelSetText( windowOptionsName.."WorldMapWindowOpacityLabel", LOC_TEXT.OPTIONS_WORLDMAP_OPACITY )
		LabelSetText( windowOptionsName.."EnableWorldMapWindowOpacityLabel", GetString( StringTables.Default.LABEL_ENABLED ))
		LabelSetText( windowOptionsName.."WorldMapWindowBackgroundOpacityLabel", LOC_TEXT.OPTIONS_WORLDMAP_BACKGROUND_OPACITY )
		LabelSetText( windowOptionsName.."EnableWorldMapWindowOpacitySliderMinLabel", L"0.0" )
		LabelSetText( windowOptionsName.."EnableWorldMapWindowOpacitySliderMaxLabel", L"1.0" )
		LabelSetText( windowOptionsName.."EnableWorldMapWindowOpacitySliderMidLabel", L"0.5" )
		LabelSetText( windowOptionsName.."EnableWorldMapWindowBackgroundOpacityLabel", GetString( StringTables.Default.LABEL_ENABLED ))
		LabelSetText( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMinLabel", L"0.0" )
		LabelSetText( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMaxLabel", L"1.0" )
		LabelSetText( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMidLabel", L"0.5" )
		ButtonSetText( windowOptionsName.."SaveButton", GetString( StringTables.Default.LABEL_SAVE ))
		ButtonSetText( windowOptionsName.."CancelButton", GetString( StringTables.Default.LABEL_CLOSE ))
	
		ReliquaryHunterOptionsWindow.UpdateOptions()
end

function ReliquaryHunterOptionsWindow.UpdateOptions()
		-- Reliquary Marker Options
		ButtonSetPressedFlag( windowOptionsName.."MarkerDisplayButton", ReliquaryHunter.Settings.HideReliquaryMarkers )
		ButtonSetPressedFlag( windowOptionsName.."MarkerTooltipDisplayButton", ReliquaryHunter.Settings.DisableReliquaryToolTips )
		
		-- World Map Options - opacity buttons
		ButtonSetPressedFlag( windowOptionsName.."EnableWorldMapWindowOpacityButton", ReliquaryHunter.Settings.WorldMapOpacityValueEnabled )
		SliderBarSetCurrentPosition( windowOptionsName.."EnableWorldMapWindowOpacitySlider", ReliquaryHunter.Settings.WorldMapOpacityValue )
		WindowSetAlpha( "EA_Window_WorldMap", ReliquaryHunter.Settings.WorldMapOpacityValue)

		ButtonSetPressedFlag( windowOptionsName.."EnableWorldMapWindowBackgroundOpacityButton", ReliquaryHunter.Settings.WorldMapBackgroundCoverOpacityValueEnabled )
		SliderBarSetCurrentPosition( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySlider", ReliquaryHunter.Settings.WorldMapBackgroundCoverOpacityValue )
		WindowSetAlpha( "EA_Window_WorldMapBackgroundCover", ReliquaryHunter.Settings.WorldMapBackgroundCoverOpacityValue)

		-- Provide player UI feedback - enable/disable World Map opacity slider bar and denote with grayed out text
		if ButtonGetPressedFlag( windowOptionsName.."EnableWorldMapWindowOpacityButton" ) then
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacityLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b ) 
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacitySliderMinLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacitySliderMaxLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacitySliderMidLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
				SliderBarSetDisabledFlag ( windowOptionsName.."EnableWorldMapWindowOpacitySlider", false )
		else
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacityLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b ) 
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacitySliderMinLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacitySliderMaxLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacitySliderMidLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b )
				SliderBarSetDisabledFlag ( windowOptionsName.."EnableWorldMapWindowOpacitySlider", true )
		end

		-- Provide player UI feedback - enable/disable World Map Background opacity slider bar and denote with grayed out text
		if ButtonGetPressedFlag( windowOptionsName.."EnableWorldMapWindowBackgroundOpacityButton" ) then
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacityLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b ) 
	  		LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMinLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMaxLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMidLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
				SliderBarSetDisabledFlag ( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySlider", false )
		else
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacityLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b ) 
	  		LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMinLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMaxLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMidLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b )
				SliderBarSetDisabledFlag ( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySlider", true )
		end

end

function ReliquaryHunterOptionsWindow.Cancel()
		WindowSetShowing( windowOptionsName, false)
end

function ReliquaryHunterOptionsWindow.Save()
		
		-- Marker Settings
		-- Get the option setting checkbox value
		ReliquaryHunter.Settings.HideReliquaryMarkers = ButtonGetPressedFlag( windowOptionsName.."MarkerDisplayButton" )
		
		-- Set the global variable based on option setting
		if ReliquaryHunter.Settings.HideReliquaryMarkers then
				bReliquariesShowing = false
		else
				bReliquariesShowing = true
		end
		
		-- Show/Hide Reliquary markers depending on option settings
		for _, rData in pairs( dataReliquaries ) do
				local markerWindowName = windowName .. "ReliquaryMarker" .. rData.id

				-- Make sure windows exist before taking action (player has not been to LoTD and opened map yet)
				if DoesWindowExist(markerWindowName)	then
						if (bReliquariesShowing == true) then
								WindowSetShowing( markerWindowName, true )
						else
								WindowSetShowing( markerWindowName, false )
						end
				end
		end
		
		ReliquaryHunter.SetOptionButtonIcon()

		-- Tooltip Settings		
		ReliquaryHunter.Settings.DisableReliquaryToolTips = ButtonGetPressedFlag( windowOptionsName.."MarkerTooltipDisplayButton" )
		
		-- Set Reliquary markers to not handle input depending on option settings, this disables mouseovers as well
		-- as solves the problem for which users may have disabled these, they are interfering with mouseover on other map icons
		for _, rData in pairs( dataReliquaries ) do
				local markerWindowName = windowName .. "ReliquaryMarker" .. rData.id

				-- Make sure windows exist before taking action (player has not been to LoTD and opened map yet)
				if DoesWindowExist(markerWindowName)	then
						if ReliquaryHunter.Settings.DisableReliquaryToolTips then
								WindowSetHandleInput(markerWindowName, false)
						else
								WindowSetHandleInput(markerWindowName, true)
						end
				end
				
		end

		-- Opacity Settings - World Map	
		ReliquaryHunter.Settings.WorldMapOpacityValueEnabled = ButtonGetPressedFlag( windowOptionsName.."EnableWorldMapWindowOpacityButton" )
		if ButtonGetPressedFlag( windowOptionsName.."EnableWorldMapWindowOpacityButton" ) then
				ReliquaryHunter.Settings.WorldMapOpacityValue = SliderBarGetCurrentPosition( windowOptionsName.."EnableWorldMapWindowOpacitySlider" )
		else
				ReliquaryHunter.Settings.WorldMapOpacityValue = opacityDefaultValueWorldMap -- set to original default value
				WindowSetAlpha("EA_Window_WorldMap", opacityDefaultValueWorldMap) -- set to original default value
		end

		-- Opacity Settings - World Map	Background
		ReliquaryHunter.Settings.WorldMapBackgroundCoverOpacityValueEnabled = ButtonGetPressedFlag( windowOptionsName.."EnableWorldMapWindowBackgroundOpacityButton" )
		if ButtonGetPressedFlag( windowOptionsName.."EnableWorldMapWindowBackgroundOpacityButton" ) then
				ReliquaryHunter.Settings.WorldMapBackgroundOpacityValue = SliderBarGetCurrentPosition( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySlider" )
		else
				ReliquaryHunter.Settings.WorldMapBackgroundOpacityValue = opacityDefaultValueWorldMapBackgroundCover -- set to original default value
				WindowSetAlpha("EA_Window_WorldMapBackgroundCover", opacityDefaultValueWorldMapBackgroundCover) -- set to original default value
		end

end

function ReliquaryHunterOptionsWindow.OnShown()
  ReliquaryHunterOptionsWindow.UpdateOptions()
end

function ReliquaryHunterOptionsWindow.OnEnableWorldMapWindowOpacityChanged()
		EA_LabelCheckButton.Toggle()

		if (ButtonGetPressedFlag( windowOptionsName.."EnableWorldMapWindowOpacityButton" ) == false ) then
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacityLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b ) 
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacitySliderMinLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacitySliderMaxLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacitySliderMidLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b )
				
				-- On not Enabled, set the savesetting value, window opacity value, and slider value back the original values and disable the slider
				ReliquaryHunter.Settings.WorldMapOpacityValue = opacityDefaultValueWorldMap
				SliderBarSetCurrentPosition( windowOptionsName.."EnableWorldMapWindowOpacitySlider", ReliquaryHunter.Settings.WorldMapOpacityValue )
				WindowSetAlpha( "EA_Window_WorldMap", ReliquaryHunter.Settings.WorldMapOpacityValue)
				SliderBarSetDisabledFlag ( windowOptionsName.."EnableWorldMapWindowOpacitySlider", true )
		
		elseif (ButtonGetPressedFlag( windowOptionsName.."EnableWorldMapWindowOpacityButton" ) == true ) then
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacityLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b ) 
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacitySliderMinLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacitySliderMaxLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowOpacitySliderMidLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
				SliderBarSetDisabledFlag ( windowOptionsName.."EnableWorldMapWindowOpacitySlider", false )
		end

end

function ReliquaryHunterOptionsWindow.OnEnableWorldMapWindowBackgroundOpacityChanged ()
		EA_LabelCheckButton.Toggle()

		if (ButtonGetPressedFlag( windowOptionsName.."EnableWorldMapWindowBackgroundOpacityButton" ) == false ) then
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacityLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b ) 
	  		LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMinLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMaxLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMidLabel", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b )

				-- On not Enabled, set the savesetting value, window opacity value, and slider value back the original values and disable the slider
				ReliquaryHunter.Settings.WorldMapBackgroundCoverOpacityValue = opacityDefaultValueWorldMapBackgroundCover
				SliderBarSetCurrentPosition( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySlider", ReliquaryHunter.Settings.WorldMapBackgroundCoverOpacityValue )
				WindowSetAlpha( "EA_Window_WorldMapBackgroundCover", ReliquaryHunter.Settings.WorldMapBackgroundCoverOpacityValue )
				SliderBarSetDisabledFlag ( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySlider", true )

		elseif (ButtonGetPressedFlag( windowOptionsName.."EnableWorldMapWindowBackgroundOpacityButton" ) == true ) then
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacityLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b ) 
	  		LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMinLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMaxLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
				LabelSetTextColor( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySliderMidLabel", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
				SliderBarSetDisabledFlag ( windowOptionsName.."EnableWorldMapWindowBackgroundOpacitySlider", false )
		end

end

function ReliquaryHunterOptionsWindow.OnWorldMapWindowOpacitySlide( slidePos )
		ReliquaryHunter.Settings.WorldMapOpacityValue = slidePos
		WindowSetAlpha("EA_Window_WorldMap", slidePos)
end

function ReliquaryHunterOptionsWindow.OnWorldMapWindowBackgroundOpacitySlide( slidePos )
		ReliquaryHunter.Settings.WorldMapBackgroundCoverOpacityValue = slidePos
		WindowSetAlpha("EA_Window_WorldMapBackgroundCover", slidePos)
end

------------------------------------------------------------------
---- Mouseovers/Tooltips
------------------------------------------------------------------

function ReliquaryHunter.InitializeMarkerTooltip()
	-- Create the tooltip window
	CreateWindow(markerToolTipWindowName, false)

	-- Set common text
	LabelSetText( markerToolTipWindowName .. "Header", LOC_TEXT.TOOLTIP_HEADER )
	LabelSetText( markerToolTipWindowName .. "RewardType", LOC_TEXT.TOOLTIP_REWARDTYPE )
	LabelSetText( markerToolTipWindowName .. "Description", LOC_TEXT.TOOLTIP_DESC )

	WindowSetAlpha( markerToolTipWindowName, alphaMarkerTooltip )

end

function ReliquaryHunter.OnMouseOverMarker()
		
		local markerWindowName = SystemData.MouseOverWindow.name
		local textCoordinates = L""

		WindowSetShowing(markerToolTipWindowName, true)

		-- Find the marker window name to match it to coordinate data to display
		for _, locData in pairs( markerTooltipData ) do
				if (locData.winName == markerWindowName) then
					textCoordinates = L"( "..locData.xPos..L", "..locData.yPos..L" )"
				end
		end

		-- Set the label to display the Reliquary marker map coordinates (localization futureproofing)
		LabelSetText( markerToolTipWindowName .. "MapCoordinates", LOC_TEXT.TOOLTIP_COORDINATES..textCoordinates )

		-- Adjust the tooltip window dimensions
		local totalHeight = 0
		local totalWidth = 0
		local widestLabel = 0
		local widthIcon = 0
		local heightIcon = 0
		local widthPadding = 20
		local heightPadding = 0

		-- Loop through each tooltip window element and get and adjust dimensions
		for k, element in pairs(markerTooltipElements) do
				local elementName = markerToolTipWindowName..element
				local widthLabel, heightLabel = 0, 0
				if element == "Icon" then
					widthIcon, heightIcon = WindowGetDimensions(elementName)
				else
					widthLabel, heightLabel = LabelGetTextDimensions(elementName)
				end
				
				if widthLabel > widestLabel then
					widestLabel = widthLabel
				end

				totalHeight = totalHeight + heightLabel

		end
		
		-- Take dimensions and add some padding for better display
		totalWidth = widestLabel + widthIcon + ( widthPadding * 2 )
		totalHeight = totalHeight + heightIcon + ( heightPadding * 2 )
		
		-- Set the tooltip window dimensions
		WindowSetDimensions( markerToolTipWindowName, totalWidth , totalHeight )

		-- Set the tooltip anchor
		ReliquaryHunter.AnchorTooltip()

end

function ReliquaryHunter.AnchorTooltip()

		local anchor = Tooltips.ANCHOR_CURSOR

    WindowClearAnchors( markerToolTipWindowName )
    WindowAddAnchor( markerToolTipWindowName, anchor.Point, anchor.RelativeTo, anchor.RelativePoint, anchor.XOffset, anchor.YOffset ) 

end

function ReliquaryHunter.OnMouseOverEndMarker()
		WindowSetShowing(markerToolTipWindowName, false)
end

function ReliquaryHunter.OnMouseOverOptionButton()

		local windowMouseOver = SystemData.MouseOverWindow.name
		local line1 = ""
		local line2 = ""
		local titleColor = Tooltips.MAP_DESC_TEXT_COLOR
		local anchorpoint = ReliquaryHunter.Tooltips.ANCHOR_ICON_WINDOW_RIGHT_OFFSET
		
		-- Construct the mouseover text based on if in LoTD
		if (GameData.Player.zone == idZoneLoTD) then
				line1 = LOC_TEXT.MOUSEOVER_OPTION_BUTTON_TITLE..L"\n"
				line2 = LOC_TEXT.MOUSEOVER_OPTION_BUTTON_DESC_INLOTD..L"\n"..LOC_TEXT.MOUSEOVER_OPTION_BUTTON_DESC_LCLICK..L"\n"..LOC_TEXT.MOUSEOVER_OPTION_BUTTON_DESC_RCLICK
		else
				line1 = LOC_TEXT.MOUSEOVER_OPTION_BUTTON_TITLE..L"\n"
				line2 = LOC_TEXT.MOUSEOVER_OPTION_BUTTON_DESC_NOTINLOTD..L"\n"..LOC_TEXT.MOUSEOVER_OPTION_BUTTON_DESC_RCLICK
		end

		Tooltips.CreateTextOnlyTooltip( windowMouseOver, nil )

		Tooltips.SetTooltipText( 1, 1, line1 )
		Tooltips.SetTooltipColorDef( 1, 1, titleColor )
		Tooltips.SetTooltipText( 2, 1, line2 )

		Tooltips.AnchorTooltip( anchorpoint )

		Tooltips.Finalize()

end
